// neo4j graph database for Audit procedure
// using Cypher Query Language

// configuration
// CALL dbms.setConfigValue('dbms.transaction.timeout', '3000s');
// :config initialNodeDisplay: 1000;

//...................... remove previous data .........................................................................
MATCH (n) DETACH DELETE n;

//.................... Project ........................................................................................
MERGE (PR1: Project {
        name: 'Production Line 2021',
        group_is_started: True,
        active_group_name: 'system1',
        active_question_node_id: '3',
        pending_evidence: True,
        collected_answers: []
    }
)
// nodes should be used instead of the complex json dicts
// the collected_answers property will be updated later

CREATE (CA1: CollectedAnswer {question_node_id: '1', answer: 'No', evidence: Null})
CREATE (CA2: CollectedAnswer {question_node_id: '2', answer: 'Yes', evidence: 'Here is the link to the data: data.pdf'})
CREATE (CA3: CollectedAnswer {question_node_id: '3', answer: 'Yes', evidence: Null})

MERGE (PR1)-[:HAS_COLLECTED_ANSWER] -> (CA1)
MERGE (PR1)-[:HAS_COLLECTED_ANSWER] -> (CA2)
MERGE (PR1)-[:HAS_COLLECTED_ANSWER] -> (CA3)

CREATE (PR2: Project {
        name: 'Production Line 2022',
        group_is_started: False,
        active_group_name: Null,
        active_question_node_id: Null,
        pending_evidence: False,
        collected_answers: []
    }
)

//.................... Group ........................................................................................
MERGE (G1: Group {name: 'system1'})
MERGE (G2: Group {name: 'system2'})
MERGE (G3: Group {name: 'system3'})
MERGE (G4: Group {name: 'system4'})
MERGE (G5: Group {name: 'system5'})


MERGE (G1)-[:HAS_FOLLOWUP] -> (G2)
MERGE (G2)-[:HAS_FOLLOWUP] -> (G3)
MERGE (G3)-[:HAS_FOLLOWUP] -> (G4)
MERGE (G4)-[:HAS_FOLLOWUP] -> (G5)


//.................... Data Loading from CSV - system1............................................................................

LOAD CSV WITH HEADERS FROM "file:///system1/questions_tasks_restructured.csv" AS csvLine
WITH csvLine WHERE csvLine.type = "MQ"
MERGE (MQ: MainQuestion {question_node_id: csvLine.qid, description: csvLine.question, group: 'system1'});

LOAD CSV WITH HEADERS FROM "file:///system1/questions_tasks_restructured.csv" AS csvLine
WITH csvLine WHERE csvLine.type = "SQ"
MERGE (SQ: SubQuestion {question_node_id: csvLine.qid, description: csvLine.question, group: 'system1'});

LOAD CSV WITH HEADERS FROM "file:///system1/questions_tasks_restructured.csv" AS csvLine
MERGE (T: Task {description: csvLine.task});

LOAD CSV WITH HEADERS FROM "file:///system1/questions_tasks_restructured.csv" AS csvLine
WITH csvLine WHERE csvLine.type = "MQ"
MERGE (MQ: MainQuestion {question_node_id: csvLine.qid, description: csvLine.question, group: 'system1'})
MERGE (T: Task {description: csvLine.task})
MERGE (MQ)-[:HAS]->(T);

LOAD CSV WITH HEADERS FROM "file:///system1/questions_tasks_restructured.csv" AS csvLine
WITH csvLine WHERE csvLine.type = "SQ"
MERGE (SQ: SubQuestion {question_node_id: csvLine.qid, description: csvLine.question, group: 'system1'})
MERGE (T: Task {description: csvLine.task})
MERGE (SQ)-[:HAS]->(T);

// main question-group relation
LOAD CSV WITH HEADERS FROM "file:///system1/questions_tasks_restructured.csv" AS csvLine
WITH csvLine WHERE csvLine.type = "MQ"
MERGE (MQ: MainQuestion {question_node_id: csvLine.qid, description: csvLine.question, group: 'system1'})
MERGE (G: Group {name: 'system1'})
MERGE (G)-[:HAS]->(MQ);

// other main question relations
LOAD CSV WITH HEADERS FROM "file:///system1/main_questions_other_relations.csv" AS csvLine
MERGE (MQ: MainQuestion {question_node_id: csvLine.qid, description: csvLine.question, group: 'system1'})

MERGE (TE: TargetEntity {name: csvLine.entiry})
MERGE (TE)-[:HAS]->(MQ)

MERGE (P: Property {name: csvLine.prop, description: csvLine.prop_desc})
MERGE (P)-[:HAS]->(MQ)

MERGE (CH: Characteristic {name: csvLine.characteristic, id: csvLine.characteristic_id})
MERGE (CH)-[:HAS]->(MQ);

// question 'Has' relations
LOAD CSV WITH HEADERS FROM "file:///system1/questions_has_rel.csv" AS csvLine
WITH csvLine WHERE csvLine.SQ_col1 IS NOT NULL
MERGE (MQ: MainQuestion {question_node_id: csvLine.col0_qid, description: csvLine.MQ_col0})
MERGE (SQ1: SubQuestion {question_node_id: csvLine.col1_qid, description: csvLine.SQ_col1})
MERGE (MQ)-[:HAS]->(SQ1);

LOAD CSV WITH HEADERS FROM "file:///system1/questions_has_rel.csv" AS csvLine
WITH csvLine WHERE csvLine.SQ_col2 IS NOT NULL
MERGE (SQ1: SubQuestion {question_node_id: csvLine.col1_qid, description: csvLine.SQ_col1})
MERGE (SQ2: SubQuestion {question_node_id: csvLine.col2_qid, description: csvLine.SQ_col2})
MERGE (SQ1)-[:HAS]->(SQ2);

LOAD CSV WITH HEADERS FROM "file:///system1/questions_has_rel.csv" AS csvLine
WITH csvLine WHERE csvLine.SQ_col3 IS NOT NULL
MERGE (SQ2: SubQuestion {question_node_id: csvLine.col2_qid, description: csvLine.SQ_col2})
MERGE (SQ3: SubQuestion {question_node_id: csvLine.col3_qid, description: csvLine.SQ_col3})
MERGE (SQ2)-[:HAS]->(SQ3);



//.................... Data Loading from CSV - system2............................................................................

LOAD CSV WITH HEADERS FROM "file:///system2/questions_tasks_restructured.csv" AS csvLine
WITH csvLine WHERE csvLine.type = "MQ"
MERGE (MQ: MainQuestion {question_node_id: csvLine.qid, description: csvLine.question, group: 'system2'});

LOAD CSV WITH HEADERS FROM "file:///system2/questions_tasks_restructured.csv" AS csvLine
WITH csvLine WHERE csvLine.type = "SQ"
MERGE (SQ: SubQuestion {question_node_id: csvLine.qid, description: csvLine.question, group: 'system2'});

LOAD CSV WITH HEADERS FROM "file:///system2/questions_tasks_restructured.csv" AS csvLine
MERGE (T: Task {description: csvLine.task});

LOAD CSV WITH HEADERS FROM "file:///system2/questions_tasks_restructured.csv" AS csvLine
WITH csvLine WHERE csvLine.type = "MQ"
MERGE (MQ: MainQuestion {question_node_id: csvLine.qid, description: csvLine.question, group: 'system2'})
MERGE (T: Task {description: csvLine.task})
MERGE (MQ)-[:HAS]->(T);

LOAD CSV WITH HEADERS FROM "file:///system2/questions_tasks_restructured.csv" AS csvLine
WITH csvLine WHERE csvLine.type = "SQ"
MERGE (SQ: SubQuestion {question_node_id: csvLine.qid, description: csvLine.question, group: 'system2'})
MERGE (T: Task {description: csvLine.task})
MERGE (SQ)-[:HAS]->(T);

// main question-group relation
LOAD CSV WITH HEADERS FROM "file:///system2/questions_tasks_restructured.csv" AS csvLine
WITH csvLine WHERE csvLine.type = "MQ"
MERGE (MQ: MainQuestion {question_node_id: csvLine.qid, description: csvLine.question, group: 'system2'})
MERGE (G: Group {name: 'system2'})
MERGE (G)-[:HAS]->(MQ);

// other main question relations
LOAD CSV WITH HEADERS FROM "file:///system2/main_questions_other_relations.csv" AS csvLine
MERGE (MQ: MainQuestion {question_node_id: csvLine.qid, description: csvLine.question, group: 'system2'})

MERGE (TE: TargetEntity {name: csvLine.entiry})
MERGE (TE)-[:HAS]->(MQ)

MERGE (P: Property {name: csvLine.prop, description: csvLine.prop_desc})
MERGE (P)-[:HAS]->(MQ)

MERGE (CH: Characteristic {name: csvLine.characteristic, id: csvLine.characteristic_id})
MERGE (CH)-[:HAS]->(MQ);

// question 'Has' relations
LOAD CSV WITH HEADERS FROM "file:///system2/questions_has_rel.csv" AS csvLine
WITH csvLine WHERE csvLine.SQ_col1 IS NOT NULL
MERGE (MQ: MainQuestion {question_node_id: csvLine.col0_qid, description: csvLine.MQ_col0})
MERGE (SQ1: SubQuestion {question_node_id: csvLine.col1_qid, description: csvLine.SQ_col1})
MERGE (MQ)-[:HAS]->(SQ1);

LOAD CSV WITH HEADERS FROM "file:///system2/questions_has_rel.csv" AS csvLine
WITH csvLine WHERE csvLine.SQ_col2 IS NOT NULL
MERGE (SQ1: SubQuestion {question_node_id: csvLine.col1_qid, description: csvLine.SQ_col1})
MERGE (SQ2: SubQuestion {question_node_id: csvLine.col2_qid, description: csvLine.SQ_col2})
MERGE (SQ1)-[:HAS]->(SQ2);

LOAD CSV WITH HEADERS FROM "file:///system2/questions_has_rel.csv" AS csvLine
WITH csvLine WHERE csvLine.SQ_col3 IS NOT NULL
MERGE (SQ2: SubQuestion {question_node_id: csvLine.col2_qid, description: csvLine.SQ_col2})
MERGE (SQ3: SubQuestion {question_node_id: csvLine.col3_qid, description: csvLine.SQ_col3})
MERGE (SQ2)-[:HAS]->(SQ3);



//.................... Data Loading from CSV - system3............................................................................

LOAD CSV WITH HEADERS FROM "file:///system3/questions_tasks_restructured.csv" AS csvLine
WITH csvLine WHERE csvLine.type = "MQ"
MERGE (MQ: MainQuestion {question_node_id: csvLine.qid, description: csvLine.question, group: 'system3'});

LOAD CSV WITH HEADERS FROM "file:///system3/questions_tasks_restructured.csv" AS csvLine
WITH csvLine WHERE csvLine.type = "SQ"
MERGE (SQ: SubQuestion {question_node_id: csvLine.qid, description: csvLine.question, group: 'system3'});

LOAD CSV WITH HEADERS FROM "file:///system3/questions_tasks_restructured.csv" AS csvLine
MERGE (T: Task {description: csvLine.task});

LOAD CSV WITH HEADERS FROM "file:///system3/questions_tasks_restructured.csv" AS csvLine
WITH csvLine WHERE csvLine.type = "MQ"
MERGE (MQ: MainQuestion {question_node_id: csvLine.qid, description: csvLine.question, group: 'system3'})
MERGE (T: Task {description: csvLine.task})
MERGE (MQ)-[:HAS]->(T);

LOAD CSV WITH HEADERS FROM "file:///system3/questions_tasks_restructured.csv" AS csvLine
WITH csvLine WHERE csvLine.type = "SQ"
MERGE (SQ: SubQuestion {question_node_id: csvLine.qid, description: csvLine.question, group: 'system3'})
MERGE (T: Task {description: csvLine.task})
MERGE (SQ)-[:HAS]->(T);

// main question-group relation
LOAD CSV WITH HEADERS FROM "file:///system3/questions_tasks_restructured.csv" AS csvLine
WITH csvLine WHERE csvLine.type = "MQ"
MERGE (MQ: MainQuestion {question_node_id: csvLine.qid, description: csvLine.question, group: 'system3'})
MERGE (G: Group {name: 'system3'})
MERGE (G)-[:HAS]->(MQ);

// other main question relations
LOAD CSV WITH HEADERS FROM "file:///system3/main_questions_other_relations.csv" AS csvLine
MERGE (MQ: MainQuestion {question_node_id: csvLine.qid, description: csvLine.question, group: 'system3'})

MERGE (TE: TargetEntity {name: csvLine.entiry})
MERGE (TE)-[:HAS]->(MQ)

MERGE (P: Property {name: csvLine.prop, description: csvLine.prop_desc})
MERGE (P)-[:HAS]->(MQ)

MERGE (CH: Characteristic {name: csvLine.characteristic, id: csvLine.characteristic_id})
MERGE (CH)-[:HAS]->(MQ);

// question 'Has' relations
LOAD CSV WITH HEADERS FROM "file:///system3/questions_has_rel.csv" AS csvLine
WITH csvLine WHERE csvLine.SQ_col1 IS NOT NULL
MERGE (MQ: MainQuestion {question_node_id: csvLine.col0_qid, description: csvLine.MQ_col0})
MERGE (SQ1: SubQuestion {question_node_id: csvLine.col1_qid, description: csvLine.SQ_col1})
MERGE (MQ)-[:HAS]->(SQ1);

LOAD CSV WITH HEADERS FROM "file:///system3/questions_has_rel.csv" AS csvLine
WITH csvLine WHERE csvLine.SQ_col2 IS NOT NULL
MERGE (SQ1: SubQuestion {question_node_id: csvLine.col1_qid, description: csvLine.SQ_col1})
MERGE (SQ2: SubQuestion {question_node_id: csvLine.col2_qid, description: csvLine.SQ_col2})
MERGE (SQ1)-[:HAS]->(SQ2);

LOAD CSV WITH HEADERS FROM "file:///system3/questions_has_rel.csv" AS csvLine
WITH csvLine WHERE csvLine.SQ_col3 IS NOT NULL
MERGE (SQ2: SubQuestion {question_node_id: csvLine.col2_qid, description: csvLine.SQ_col2})
MERGE (SQ3: SubQuestion {question_node_id: csvLine.col3_qid, description: csvLine.SQ_col3})
MERGE (SQ2)-[:HAS]->(SQ3);


//.................... Data Loading from CSV - system4............................................................................

LOAD CSV WITH HEADERS FROM "file:///system4/questions_tasks_restructured.csv" AS csvLine
WITH csvLine WHERE csvLine.type = "MQ"
MERGE (MQ: MainQuestion {question_node_id: csvLine.qid, description: csvLine.question, group: 'system4'});

LOAD CSV WITH HEADERS FROM "file:///system4/questions_tasks_restructured.csv" AS csvLine
WITH csvLine WHERE csvLine.type = "SQ"
MERGE (SQ: SubQuestion {question_node_id: csvLine.qid, description: csvLine.question, group: 'system4'});

LOAD CSV WITH HEADERS FROM "file:///system4/questions_tasks_restructured.csv" AS csvLine
MERGE (T: Task {description: csvLine.task});

LOAD CSV WITH HEADERS FROM "file:///system4/questions_tasks_restructured.csv" AS csvLine
WITH csvLine WHERE csvLine.type = "MQ"
MERGE (MQ: MainQuestion {question_node_id: csvLine.qid, description: csvLine.question, group: 'system4'})
MERGE (T: Task {description: csvLine.task})
MERGE (MQ)-[:HAS]->(T);

LOAD CSV WITH HEADERS FROM "file:///system4/questions_tasks_restructured.csv" AS csvLine
WITH csvLine WHERE csvLine.type = "SQ"
MERGE (SQ: SubQuestion {question_node_id: csvLine.qid, description: csvLine.question, group: 'system4'})
MERGE (T: Task {description: csvLine.task})
MERGE (SQ)-[:HAS]->(T);

// main question-group relation
LOAD CSV WITH HEADERS FROM "file:///system4/questions_tasks_restructured.csv" AS csvLine
WITH csvLine WHERE csvLine.type = "MQ"
MERGE (MQ: MainQuestion {question_node_id: csvLine.qid, description: csvLine.question, group: 'system4'})
MERGE (G: Group {name: 'system4'})
MERGE (G)-[:HAS]->(MQ);

// other main question relations
LOAD CSV WITH HEADERS FROM "file:///system4/main_questions_other_relations.csv" AS csvLine
MERGE (MQ: MainQuestion {question_node_id: csvLine.qid, description: csvLine.question, group: 'system4'})

MERGE (TE: TargetEntity {name: csvLine.entiry})
MERGE (TE)-[:HAS]->(MQ)

MERGE (P: Property {name: csvLine.prop, description: csvLine.prop_desc})
MERGE (P)-[:HAS]->(MQ)

MERGE (CH: Characteristic {name: csvLine.characteristic, id: csvLine.characteristic_id})
MERGE (CH)-[:HAS]->(MQ);

// question 'Has' relations
LOAD CSV WITH HEADERS FROM "file:///system4/questions_has_rel.csv" AS csvLine
WITH csvLine WHERE csvLine.SQ_col1 IS NOT NULL
MERGE (MQ: MainQuestion {question_node_id: csvLine.col0_qid, description: csvLine.MQ_col0})
MERGE (SQ1: SubQuestion {question_node_id: csvLine.col1_qid, description: csvLine.SQ_col1})
MERGE (MQ)-[:HAS]->(SQ1);

LOAD CSV WITH HEADERS FROM "file:///system4/questions_has_rel.csv" AS csvLine
WITH csvLine WHERE csvLine.SQ_col2 IS NOT NULL
MERGE (SQ1: SubQuestion {question_node_id: csvLine.col1_qid, description: csvLine.SQ_col1})
MERGE (SQ2: SubQuestion {question_node_id: csvLine.col2_qid, description: csvLine.SQ_col2})
MERGE (SQ1)-[:HAS]->(SQ2);

LOAD CSV WITH HEADERS FROM "file:///system4/questions_has_rel.csv" AS csvLine
WITH csvLine WHERE csvLine.SQ_col3 IS NOT NULL
MERGE (SQ2: SubQuestion {question_node_id: csvLine.col2_qid, description: csvLine.SQ_col2})
MERGE (SQ3: SubQuestion {question_node_id: csvLine.col3_qid, description: csvLine.SQ_col3})
MERGE (SQ2)-[:HAS]->(SQ3);



//.................... Data Loading from CSV - system5............................................................................

LOAD CSV WITH HEADERS FROM "file:///system5/questions_tasks_restructured.csv" AS csvLine
WITH csvLine WHERE csvLine.type = "MQ"
MERGE (MQ: MainQuestion {question_node_id: csvLine.qid, description: csvLine.question, group: 'system5'});

LOAD CSV WITH HEADERS FROM "file:///system5/questions_tasks_restructured.csv" AS csvLine
WITH csvLine WHERE csvLine.type = "SQ"
MERGE (SQ: SubQuestion {question_node_id: csvLine.qid, description: csvLine.question, group: 'system5'});

LOAD CSV WITH HEADERS FROM "file:///system5/questions_tasks_restructured.csv" AS csvLine
MERGE (T: Task {description: csvLine.task});

LOAD CSV WITH HEADERS FROM "file:///system5/questions_tasks_restructured.csv" AS csvLine
WITH csvLine WHERE csvLine.type = "MQ"
MERGE (MQ: MainQuestion {question_node_id: csvLine.qid, description: csvLine.question, group: 'system5'})
MERGE (T: Task {description: csvLine.task})
MERGE (MQ)-[:HAS]->(T);

LOAD CSV WITH HEADERS FROM "file:///system5/questions_tasks_restructured.csv" AS csvLine
WITH csvLine WHERE csvLine.type = "SQ"
MERGE (SQ: SubQuestion {question_node_id: csvLine.qid, description: csvLine.question, group: 'system5'})
MERGE (T: Task {description: csvLine.task})
MERGE (SQ)-[:HAS]->(T);

// main question-group relation
LOAD CSV WITH HEADERS FROM "file:///system5/questions_tasks_restructured.csv" AS csvLine
WITH csvLine WHERE csvLine.type = "MQ"
MERGE (MQ: MainQuestion {question_node_id: csvLine.qid, description: csvLine.question, group: 'system5'})
MERGE (G: Group {name: 'system5'})
MERGE (G)-[:HAS]->(MQ);

// other main question relations
LOAD CSV WITH HEADERS FROM "file:///system5/main_questions_other_relations.csv" AS csvLine
MERGE (MQ: MainQuestion {question_node_id: csvLine.qid, description: csvLine.question, group: 'system5'})

MERGE (TE: TargetEntity {name: csvLine.entiry})
MERGE (TE)-[:HAS]->(MQ)

MERGE (P: Property {name: csvLine.prop, description: csvLine.prop_desc})
MERGE (P)-[:HAS]->(MQ)

MERGE (CH: Characteristic {name: csvLine.characteristic, id: csvLine.characteristic_id})
MERGE (CH)-[:HAS]->(MQ);

// question 'Has' relations
LOAD CSV WITH HEADERS FROM "file:///system5/questions_has_rel.csv" AS csvLine
WITH csvLine WHERE csvLine.SQ_col1 IS NOT NULL
MERGE (MQ: MainQuestion {question_node_id: csvLine.col0_qid, description: csvLine.MQ_col0})
MERGE (SQ1: SubQuestion {question_node_id: csvLine.col1_qid, description: csvLine.SQ_col1})
MERGE (MQ)-[:HAS]->(SQ1);

LOAD CSV WITH HEADERS FROM "file:///system5/questions_has_rel.csv" AS csvLine
WITH csvLine WHERE csvLine.SQ_col2 IS NOT NULL
MERGE (SQ1: SubQuestion {question_node_id: csvLine.col1_qid, description: csvLine.SQ_col1})
MERGE (SQ2: SubQuestion {question_node_id: csvLine.col2_qid, description: csvLine.SQ_col2})
MERGE (SQ1)-[:HAS]->(SQ2);

LOAD CSV WITH HEADERS FROM "file:///system5/questions_has_rel.csv" AS csvLine
WITH csvLine WHERE csvLine.SQ_col3 IS NOT NULL
MERGE (SQ2: SubQuestion {question_node_id: csvLine.col2_qid, description: csvLine.SQ_col2})
MERGE (SQ3: SubQuestion {question_node_id: csvLine.col3_qid, description: csvLine.SQ_col3})
MERGE (SQ2)-[:HAS]->(SQ3);


// should put active group and question at end
//............... active Question and group..................................................
MERGE (PR1: Project {
        name: 'Production Line 2021'
    }
)
MERGE (G1: Group {name: 'system1'})
MERGE (MQ1: MainQuestion {question_node_id: '1001', group: 'system1'})
MERGE (PR1)-[:ACTIVE_QUESTION] -> (MQ1)
MERGE (PR1)-[:ACTIVE_GROUP] -> (G1);
