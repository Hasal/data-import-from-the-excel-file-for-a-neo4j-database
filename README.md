## Data import from the excel file - for a neo4j database

Several different scripts are needed to restructure the data (different restructuring methods for importing questions/main/sun questions, tasks, Property, Characteristic, and their relationships etc.)

#### Steps for creating the database,

1. Place the audit_prequestions, system1, data_design, data_integration, data_processing, data_store, external_data_acquisition, measurement_system, presentation folders inside the neo4j import directory `<neo4j-home>/import`

2. Execute the cypher queries in graph.cypher